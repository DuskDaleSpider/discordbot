﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace DiscordBot
{
    class Program
    {

        static void Main(string[] args)
        {
            var client = new DiscordClient();
            List<Command> commands = new List<Command>();

            //write log messages
            client.LogMessage += (s, e) => Console.WriteLine($"[{e.Severity}] {e.Source}: {e.Message}");

            //add default commands
            commands.Add(new Command("!hey", "Hey there, [UserName]!"));

            client.MessageReceived += async (s, e) =>
            {
                //make sure it's not this bots message
                if (!e.Message.IsAuthor)
                {
                    var message = e.Message;
                    var messageText = e.Message.Text;
                    var messageArgs = Helpers.CommandLineToArgs(messageText);

                    if (messageArgs[0] == "!addcmd")
                    {
                        try
                        {
                            commands.Add(new Command(messageArgs[1], messageArgs[2]));
                        }
                        catch (IndexOutOfRangeException err)
                        {
                            await client.SendMessage(e.Channel, "Not enough arguments to add command");
                        }
                    }

                    for (int i = 0; i < commands.Count; i++)
                    {
                        if (messageArgs[0] == commands[i].CommandMessage)
                        {
                            string output = commands[i].Output;
                            output = output.Replace("[UserName]", Discord.Mention.User(message.User));
                            if(output.IndexOf("[Target]") >= 0)
                            {
                                string target = messageArgs[1];
                                var user = e.Server.Members.First(name => name.Name == target);
                                output = output.Replace("[Target]", Discord.Mention.User(user));
                            }

                            await client.SendMessage(e.Channel, output);
                        }
                    }
                }
            };

            client.UserPresenceUpdated += async (s, e) =>
            {
                Console.Out.WriteLine($"{e.User.Name}: {e.User.Status}");
                if(e.User.Status == "online")
                {
                    await client.SendMessage(e.Server.DefaultChannel, $"Hey there, {Discord.Mention.User(e.User)}");
                }
            };

            //Start the bot
            client.Run(async () =>
            {

                await client.Connect("crzypersn@gmail.com", "1Starcraft1");

                if (client.AllServers.Any())
                {
                    Console.Out.WriteLine("Connected to Discord!");
                    foreach(var server in client.AllServers)
                    {
                        Console.Out.WriteLine(server.Name + ":");
                        foreach(var channel in server.Channels)
                        {
                            Console.Out.WriteLine("     " + channel.Name);
                        }
                        Console.Out.WriteLine("Default Channel: " + server.DefaultChannel.Name);
                    }
                }
            });
        }

    }
}
