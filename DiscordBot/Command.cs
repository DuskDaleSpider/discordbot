﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot
{
    class Command
    {

        public string CommandMessage {
            get; set;
        }

        public string[] CommandArguments {
            get; set;
        }

        public string Output {
            get; set;
        }

        public Command(string CommandMessage, string Output)
        {
            this.CommandMessage = CommandMessage;
            this.Output = Output;
        }
    }
}
